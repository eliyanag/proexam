<?php

use yii\db\Migration;

/**
 * Handles adding CategoryId to table `user`.
 */
class m170716_150613_add_CategoryId_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'CategotyId', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'CategotyId');
    }
}
