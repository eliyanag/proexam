<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity_`.
 */
class m170716_151901_create_activity__table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity_', [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'title' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity_');
    }
}
